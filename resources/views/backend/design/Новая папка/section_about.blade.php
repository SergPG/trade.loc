<!-- Section -->
				<section class="md-section">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 ">
								
								<!-- title-01 -->
								<div class="title-01 title-01__style-03 md-text-left">
									<h6 class="title-01__subTitle">about</h6>
									<h2 class="title-01__title">We Are The Leaders In This Industry</h2>
									<div>Nam suscipit nisi risus, et porttitor metus molestie a. Phasellus id quam id turpis suscipit pretium. Maecenas ultrices, lacus ut accumsan maximus, odio augue rhoncus augue, vulputate maximus mi sapien sed nisl. Sed fermentum congue orci sed lacinia. Nulla nunc purus, consectetur</div>
								</div><!-- End / title-01 -->
								
								<div class="row">
									<div class="col-sm-4 ">
										
										<!-- box-number -->
										<div class="box-number">
											<div class="box-number__number">
												<h2 class="js-counter" data-counter-time="2000" data-counter-delay="10">99</h2>
											</div>
											<div class="box-number__description">Happy clients</div>
										</div><!-- End / box-number -->
										
									</div>
									<div class="col-sm-4 ">
										
										<!-- box-number -->
										<div class="box-number">
											<div class="box-number__number">
												<h2 class="js-counter" data-counter-time="2000" data-counter-delay="10">1200</h2>
											</div>
											<div class="box-number__description">Message per Day</div>
										</div><!-- End / box-number -->
										
									</div>
									<div class="col-sm-4 ">
										
										<!-- box-number -->
										<div class="box-number">
											<div class="box-number__number">
												<h2 class="js-counter" data-counter-time="2000" data-counter-delay="10">15</h2>
											</div>
											<div class="box-number__description">Awards</div>
										</div><!-- End / box-number -->
										
									</div>
								</div>
							</div>
							<div class="col-lg-6 ">
								<div class="js-consult-slider">
									
									<!-- carousel__element owl-carousel -->
									<div class="carousel__element owl-carousel" data-options='{"items":1,"loop":true,"dots":false,"nav":false,"margin":30,"responsive":{"0":{"items":2},"576":{"items":3},"992":{"items":1}}}'>
										<div class="image-full"><img src="{{ asset('storage') }}/img/image-01.jpg" alt=""></div>
										<div class="image-full"><img src="{{ asset('storage') }}/img/image-02.jpg" alt=""></div>
									</div><!-- End / carousel__element owl-carousel -->
									
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End / Section -->