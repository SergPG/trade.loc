	<!-- Section -->
				<section class="md-section md-skin-dark js-consult-form" style="background-image:url('{{ asset('storage') }}/img/backgrounds/1.jpg');">
					<div class="md-overlay"></div>
					<div class="container">
						<div class="row">
							<div class="col-lg-8 offset-0 offset-sm-0 offset-md-0 offset-lg-2 ">
								
								<!-- title-01 -->
								<div class="title-01 title-01__style-02">
									<h2 class="title-01__title">Contact With Us via Hot Line</h2>
									<div>Sed ante nisl, fermentum et facilisis in, maximus </div>
								</div><!-- End / title-01 -->
								
								<div class="consult-phone">(+88) 242 2323 777</div>
							</div>
						</div>
						
						<!-- form-01 -->
						<div class="form-01 consult-form js-consult-form__content">
							<h2 class="form-01__title">Give Us Your Feedback</h2>
							<form class="form-01__form">
								<div class="form__item form__item--03">
									<input type="text" name="name" placeholder="Your name"/>
								</div>
								<div class="form__item form__item--03">
									<input type="text" name="phone" placeholder="Your Phone"/>
								</div>
								<div class="form__item form__item--03">
									<input type="email" name="email" placeholder="Your Email"/>
								</div>
								<div class="form__item">
									<textarea rows="3" name="Your message" placeholder="Your message"></textarea>
								</div>
								<div class="form__button"><a class="btn btn-primary btn-w180" href="#">send message</a>
								</div>
							</form>
						</div><!-- End / form-01 -->
						
					</div>
				</section>
				<!-- End / Section -->