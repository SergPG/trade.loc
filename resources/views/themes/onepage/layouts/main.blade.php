<!DOCTYPE html>
<!-- ==============================
    Project:        Metronic "Aitonepage" Frontend Freebie - Responsive HTML Template Based On Twitter Bootstrap 3.3.4
    Version:        1.0
    Author:         KeenThemes
    Primary use:    Corporate, Business Themes.
    Email:          support@keenthemes.com
    Follow:         http://www.twitter.com/keenthemes
    Like:           http://www.facebook.com/keenthemes
    Website:        http://www.keenthemes.com
    Premium:        Premium Metronic Admin Theme: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
================================== -->
<html lang="en" class="no-js">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Metronic "Aitonepage" Frontend Freebie</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <!-- GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
        



        <link href="{{ asset('themes/'.$theme) }}/vendor/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('themes/'.$theme) }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!-- PAGE LEVEL PLUGIN STYLES -->
        <link href="{{ asset('themes/'.$theme) }}/css/animate.css" rel="stylesheet">
        <link href="{{ asset('themes/'.$theme) }}/vendor/swiper/css/swiper.min.css" rel="stylesheet" type="text/css"/>

        <!-- THEME STYLES -->
        <link href="{{ asset('themes/'.$theme) }}/css/layout.min.css" rel="stylesheet" type="text/css"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->

    <!-- BODY -->
    <body id="body" data-spy="scroll" data-target=".header">


    <!-- Content -->    
        @yield('content')
    <!-- END Content -->

     

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

        <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- CORE PLUGINS -->
        <script src="{{ asset('themes/'.$theme) }}/vendor/jquery.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- PAGE LEVEL PLUGINS -->
        <script src="{{ asset('themes/'.$theme) }}/vendor/jquery.easing.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/jquery.back-to-top.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/jquery.smooth-scroll.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/jquery.wow.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/swiper/js/swiper.jquery.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/masonry/jquery.masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/vendor/masonry/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U"></script>

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('themes/'.$theme) }}/js/layout.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/js/components/wow.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/js/components/swiper.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/js/components/masonry.min.js" type="text/javascript"></script>
        <script src="{{ asset('themes/'.$theme) }}/js/components/google-map.min.js" type="text/javascript"></script>

    </body>
    <!-- END BODY -->
</html>