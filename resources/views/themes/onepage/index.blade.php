@extends('themes.'.$theme.'.layouts.main')

@section('content')   


 <!--========== HEADER ==========-->
    @include('themes.'.$theme.'.design.header') 
 <!--========== END HEADER ==========-->

 <!--========== SLIDER ==========-->
    @include('themes.'.$theme.'.design.slides') 
 <!--========== SLIDER ==========-->

 <!--========== PAGE LAYOUT ==========-->
    <!-- About -->
         @include('themes.'.$theme.'.design.section_about') 
    <!-- End About -->

    <!-- Latest Products -->
         @include('themes.'.$theme.'.design.section_products')         
    <!-- End Latest Products -->

    <!-- Pricing -->
         @include('themes.'.$theme.'.design.section_pricing')        
    <!-- End Pricing -->

    <!-- Work -->
         @include('themes.'.$theme.'.design.section_work')            
    <!-- End Work -->

    <!-- Service -->
         @include('themes.'.$theme.'.design.section_services')  
    <!-- End Service -->
            
    <!-- Contact -->
         @include('themes.'.$theme.'.design.section_contact')     
    <!-- End Contact -->
<!--========== END PAGE LAYOUT ==========-->

<!--========== FOOTER ==========-->
         @include('themes.'.$theme.'.design.footer')    
       
<!--========== END FOOTER ==========-->

@endsection
