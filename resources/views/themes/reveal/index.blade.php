@extends('themes.'.$theme.'.layouts.main')

@section('content')   

<!--========== Top Bar ==========-->
    @include('themes.'.$theme.'.design.top_bar') 
<!--========== END Top Bar ==========-->

<!--========== Header ==========-->
    @include('themes.'.$theme.'.design.header') 
<!--========== END Header ==========-->

<!--==========  Intro Section ==========-->
    @include('themes.'.$theme.'.design.intro') 
<!--========== END  Intro Section ==========-->


  <main id="main">

 <!--==========  About Section ==========-->
    @include('themes.'.$theme.'.design.section_about') 
 <!--========== END  About Section ==========-->

 <!--==========  Services Section ==========-->
    @include('themes.'.$theme.'.design.section_services') 
 <!--========== END  Services Section ==========-->  

 <!--==========  Clients Section ==========-->
    @include('themes.'.$theme.'.design.section_clients') 
 <!--========== END  Clients Section ==========-->  

 <!--==========   Our Portfolio Section ==========-->
    @include('themes.'.$theme.'.design.section_portfolio') 
 <!--========== END Our Portfolio Section ==========-->  

 <!--==========   Testimonials Section ==========-->
    @include('themes.'.$theme.'.design.section_testimonials') 
 <!--========== END Testimonials Section ==========-->  

 <!--==========   Call To Action Section ==========-->
    @include('themes.'.$theme.'.design.call_to_action') 
 <!--========== END Call To Action Section ==========-->  

 <!--==========   Our Team Section ==========-->
    @include('themes.'.$theme.'.design.section_team') 
 <!--========== END Our Team Section ==========-->  

 <!--==========   Contact Section ==========-->
    @include('themes.'.$theme.'.design.section_contact') 
 <!--========== END Contact Section ==========-->  

  </main>

  <!--==========   Footer ==========-->
    @include('themes.'.$theme.'.design.footer') 
 <!--========== END Footer ==========-->  

@endsection
