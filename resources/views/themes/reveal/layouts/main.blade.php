<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Reveal Bootstrap Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset('storage/themes/'.$theme)}}/img/favicon.png" rel="icon">
  <link href="{{ asset('storage/themes/'.$theme)}}/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('themes/'.$theme) }}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('themes/'.$theme) }}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="{{ asset('themes/'.$theme) }}/lib/animate/animate.min.css" rel="stylesheet">
  <link href="{{ asset('themes/'.$theme) }}/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="{{ asset('themes/'.$theme) }}/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="{{ asset('themes/'.$theme) }}/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="{{ asset('themes/'.$theme) }}/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('themes/'.$theme) }}/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">


    <!-- Content -->    
        @yield('content')
    <!-- END Content -->

     

  <!-- Back To Top --> 
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('themes/'.$theme) }}/lib/jquery/jquery.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/jquery/jquery-migrate.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/easing/easing.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/superfish/hoverIntent.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/superfish/superfish.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/wow/wow.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="{{ asset('themes/'.$theme) }}/lib/sticky/sticky.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('themes/'.$theme) }}/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('themes/'.$theme) }}/js/main.js"></script>

</body>
</html>
