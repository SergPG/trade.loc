@extends('themes.'.$theme.'.layouts.main')

@section('content')   

<!--========== HEADER ==========-->
    @include('themes.'.$theme.'.design.header') 
 
<!--========== END HEADER ==========-->

<!--========== Home Section ==========-->
    @include('themes.'.$theme.'.design.section_home') 
 
<!--========== END Home Section ==========-->

<!--========== About Section ==========-->
    @include('themes.'.$theme.'.design.section_about') 
 
<!--========== END About Section ==========-->

<!--========== Service Section ==========-->
    @include('themes.'.$theme.'.design.section_service') 
 
<!--========== END Service Section ==========-->

<!--========== Experience Section ==========-->
    @include('themes.'.$theme.'.design.section_experience') 
 
<!--========== END Experience Section ==========-->

<!--========== Education Section ==========-->
    @include('themes.'.$theme.'.design.section_education') 
 
<!--========== END Education Section ==========-->

<!--========== Quotes Section ==========-->
    @include('themes.'.$theme.'.design.section_quotes') 
 
<!--========== END Quotes Section ==========-->

<!--========== Contact Section ==========-->
    @include('themes.'.$theme.'.design.section_contact') 
 
<!--========== END Contact Section ==========-->

<!--========== Footer Section ==========-->
    @include('themes.'.$theme.'.design.footer') 
 
<!--========== END Footer Section ==========-->

@endsection
