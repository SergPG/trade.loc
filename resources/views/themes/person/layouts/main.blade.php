
<!-- {{ asset('themes/'.$theme) }}
 -->
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="templatemo">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Stimulus HTML CSS Template</title>
<!--
Stimulus Template
http://www.templatemo.com/tm-498-stimulus
-->
<link rel="stylesheet" href="{{ asset('themes/'.$theme) }}/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('themes/'.$theme) }}/css/animate.css">
<link rel="stylesheet" href="{{ asset('themes/'.$theme) }}/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('themes/'.$theme) }}/css/templatemo-style.css">

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">

</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">



    <!-- Content -->    
        @yield('content')
    <!-- END Content -->

     

<!-- SCRIPTS -->

<script src="{{ asset('themes/'.$theme) }}/js/jquery.js"></script>
<script src="{{ asset('themes/'.$theme) }}/js/bootstrap.min.js"></script>
<script src="{{ asset('themes/'.$theme) }}/js/jquery.parallax.js"></script>
<script src="{{ asset('themes/'.$theme) }}/js/smoothscroll.js"></script>
<script src="{{ asset('themes/'.$theme) }}/js/wow.min.js"></script>
<script src="{{ asset('themes/'.$theme) }}/js/custom.js"></script>

</body>
</html>