<!-- Quotes Section -->

<section id="quotes" class="parallax-section" style="background: url('{{ asset('storage/themes/'.$theme.'/img/'.'quotes-bg.jpg') }} ') 50% 0 repeat-y fixed;">
     <div class="overlay"></div>50% 0 repeat-y fixed
     <div class="container">
          <div class="row">

               <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <i class="wow fadeInUp fa fa-star" data-wow-delay="0.6s"></i>
                    <h2 class="wow fadeInUp" data-wow-delay="0.8s">Proin lobortis eu diam et facilisis. Fusce nisi nibh, molestie in vestibulum quis, auctor et orci.</h2>
                    <p class="wow fadeInUp" data-wow-delay="1s">Curabitur at pulvinar ante. Duis dui urna, faucibus eget felis eu, iaculis congue sem.</p>
               </div>

          </div>
     </div>
</section>
