<!-- Experience Section -->

<section id="experience" class="parallax-section">
     <div class="container">
          <div class="row">

               <div class="col-md-6 col-sm-6">
                    <div class="background-image experience-img" style="background: url('{{ asset('storage/themes/'.$theme.'/img/'.'experience-img.jpg') }} ') no-repeat;" ></div>
               </div>

               <div class="col-md-6 col-sm-6">
                    <div class="color-white experience-thumb">
                         <div class="wow fadeInUp section-title" data-wow-delay="0.8s">
                              <h1>My Experiences</h1>
                              <p class="color-white">Previous companies and my tasks</p>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                              <div class="media-object media-left">
                                   <i class="fa fa-laptop"></i>
                              </div>
                              <div class="media-body">
                                   <h3 class="media-heading">Graphic Designer <small>2014 Jul - 2015 Sep</small></h3>
                                   <p class="color-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                              <div class="media-object media-left">
                                   <i class="fa fa-laptop"></i>
                              </div>
                              <div class="media-body">
                                   <h3 class="media-heading">Web Designer <small>2015 Oct - 2017 Jan</small></h3>
                                   <p class="color-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                              </div>
                         </div>

                    </div>
               </div>

          </div>
     </div>
</section>
