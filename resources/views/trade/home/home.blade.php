@extends('trade.layouts.frontend')

@section('content')    

  <!-- Content-->
	<div class="md-content">
     
			@include('trade.design.main_slider') 
				
				
		<!-- Service-->
       
       @include('trade.design.section_textbox') 

       @include('trade.design.section_iconbox') 
				
		<!-- END Service-->	
				
				
		<!-- About-->
			
		  @include('trade.design.section_about') 	
   
    <!-- END About-->
		

		<!-- Contact us-->

		 @include('trade.design.section_contact_us') 	
				
		<!-- END Contact us-->	
				
				
		<!-- What’s Client Say ?-->

			@include('trade.design.section_testimonials') 	
				
		<!-- END What’s Client Say ?-->		

				
		<!-- Our partner-->

			@include('trade.design.section_partners') 	

		<!-- END Our partner-->		
				
				
		<!-- Latest Blogs -->

		   @include('trade.design.section_latest_blogs') 
		
		<!-- END Latest Blogs            -->	
				

			
			</div>
			<!-- End / Content-->
   
@endsection
