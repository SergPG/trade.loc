@extends('trade.layouts.frontend')




@section('content')    

  <!-- Content-->
	<div class="md-content">
     
			
				
				
		<!-- About-->
		<!-- Section -->
				<section class="md-section">
					<div class="container">
						<div class="row">
							<div class="col-lg-10 col-xl-8 offset-0 offset-sm-0 offset-md-0 offset-lg-1 offset-xl-2 ">
								
								<!-- title-01 -->
								<div class="title-01 title-01__style-04">
									<h6 class="title-01__subTitle">about</h6>
									<h2 class="title-01__title">We Are The Leaders In This Industry</h2>
									<div>Curabitur elementum urna augue, eu porta purus gravida in. Cras consectetur, lorem a cursus vestibulum, ligula purus iaculis nulla, in dignissim risus turpis id justo. Sed eleifend ante et ligula eleifend ultricies. Fusce porta feugiat nisl, eget faucibus augue ultrices et. Nullam id leo fringilla, viverra est ut, faucibus lectus. Integer vel finibus ex, et convallis enim. Curabitur tristique convallis neque a eleifend. Donec mattis leo in velit scelerisque, sit amet ultricies erat mollis. Suspendisse potenti. Etiam commodo ipsum placerat, ornare libero a, consequat justo.</div>
								</div><!-- End / title-01 -->
								
							</div>
						</div>
					</div>
				</section>
				<!-- End / Section -->
				
				
				<!-- Section -->
				<section class="md-section consult-background">
					<div class="container">
						<div class="row">
							<div class="col-lg-10 col-xl-8 offset-0 offset-sm-0 offset-md-0 offset-lg-1 offset-xl-2 ">
								
								<!-- title-01 -->
								<div class="title-01 title-01__style-04">
									<h6 class="title-01__subTitle">awardS</h6>
									<h2 class="title-01__title">This Is What We Get</h2>
								</div><!-- End / title-01 -->
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-3 ">
								
								<!-- iconbox -->
								<div class="iconbox iconbox__style-03">
									<div class="iconbox__icon"><i class="ti-cup"></i></div>
									<div>
										<h2 class="iconbox__title"><a href="#">The Myth of Ugly Design</a></h2>
										<div class="iconbox__description">Duis porttitor libero ac egestas euismod. Maecenas quis felis turpis. Nulla quis turpis sed augue eg</div>
									</div>
								</div><!-- End / iconbox -->
								
							</div>
							<div class="col-md-6 col-lg-3 ">
								
								<!-- iconbox -->
								<div class="iconbox iconbox__style-03">
									<div class="iconbox__icon"><i class="ti-crown"></i></div>
									<div>
										<h2 class="iconbox__title"><a href="#">How to Create and Manage SVG Sprites</a></h2>
										<div class="iconbox__description">Sed ante nisl, fermentum et facilisis in, maximus sed ipsum. Cras hendrerit feugiat eros, ut fringil</div>
									</div>
								</div><!-- End / iconbox -->
								
							</div>
							<div class="col-md-6 col-lg-3 ">
								
								<!-- iconbox -->
								<div class="iconbox iconbox__style-03">
									<div class="iconbox__icon"><i class="ti-world"></i></div>
									<div>
										<h2 class="iconbox__title"><a href="#">Best National Locations</a></h2>
										<div class="iconbox__description">Etiam non varius justo, vel tempor mi. Nulla facilisi. Fusce at tortor arcu. Suspendisse maximus ac </div>
									</div>
								</div><!-- End / iconbox -->
								
							</div>
							<div class="col-md-6 col-lg-3 ">
								
								<!-- iconbox -->
								<div class="iconbox iconbox__style-03">
									<div class="iconbox__icon"><i class="ti-shine"></i></div>
									<div>
										<h2 class="iconbox__title"><a href="#">How to Master Microcopy</a></h2>
										<div class="iconbox__description">Nam elit ligula, egestas et ornare non, viverra eu justo. Aliquam ornare lectus ut pharetra dictum. </div>
									</div>
								</div><!-- End / iconbox -->
								
							</div>
						</div>
					</div>
				</section>
				<!-- End / Section -->
				
				<!-- Our Advisors-->
				
				<!-- Section -->
				<section class="md-section" style="padding-bottom:0;">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-lg-8 offset-0 offset-sm-0 offset-md-2 offset-lg-2 ">
								
								<!-- title-01 -->
								<div class="title-01">
									<h2 class="title-01__title">Our Advisors</h2>
								</div><!-- End / title-01 -->
								
							</div>
						</div>
						<div class="consult-slide">
							
							<!-- carousel__element owl-carousel -->
							<div class="carousel__element owl-carousel" data-options='{"loop":true,"dots":true,"nav":false,"margin":30,"responsive":{"0":{"items":1},"768":{"items":2},"992":{"items":3}}}'>
								
								<!-- textbox -->
								<div class="textbox textbox__style-02">
									<div class="textbox__image"><a href="#"><img src="assets/img/avatars/1.jpg" alt=""/></a></div>
									<div class="textbox__body">
										<h2 class="textbox__title"><a href="#">Ashley Mills</a></h2>
										<div class="textbox__description">Nam elit ligula, egestas et ornare non, viverra eu justo. Aliquam ornare lectus ut pharetra dictum. </div>
											
											<!-- social-01 -->
											<div class="social-01">
												<nav class="social-01__navSocial"><a class="social-01__item" href="#"><i class="fa fa-facebook"></i></a><a class="social-01__item" href="#"><i class="fa fa-skype"></i></a><a class="social-01__item" href="#"><i class="fa fa-twitter"></i></a><a class="social-01__item" href="#"><i class="fa fa-instagram"></i></a>
												</nav>
											</div><!-- End / social-01 -->
											
									</div>
								</div><!-- End / textbox -->
								
								
								<!-- textbox -->
								<div class="textbox textbox__style-02">
									<div class="textbox__image"><a href="#"><img src="assets/img/avatars/2.jpg" alt=""/></a></div>
									<div class="textbox__body">
										<h2 class="textbox__title"><a href="#">Bruce Powell</a></h2>
										<div class="textbox__description">Etiam non varius justo, vel tempor mi. Nulla facilisi. Fusce at tortor arcu. Suspendisse maximus ac </div>
											
											<!-- social-01 -->
											<div class="social-01">
												<nav class="social-01__navSocial"><a class="social-01__item" href="#"><i class="fa fa-facebook"></i></a><a class="social-01__item" href="#"><i class="fa fa-skype"></i></a><a class="social-01__item" href="#"><i class="fa fa-twitter"></i></a><a class="social-01__item" href="#"><i class="fa fa-instagram"></i></a>
												</nav>
											</div><!-- End / social-01 -->
											
									</div>
								</div><!-- End / textbox -->
								
								
								<!-- textbox -->
								<div class="textbox textbox__style-02">
									<div class="textbox__image"><a href="#"><img src="assets/img/avatars/3.jpg" alt=""/></a></div>
									<div class="textbox__body">
										<h2 class="textbox__title"><a href="#">Ann Fowler</a></h2>
										<div class="textbox__description">Sed ante nisl, fermentum et facilisis in, maximus sed ipsum. Cras hendrerit feugiat eros, ut fringil</div>
											
											<!-- social-01 -->
											<div class="social-01">
												<nav class="social-01__navSocial"><a class="social-01__item" href="#"><i class="fa fa-facebook"></i></a><a class="social-01__item" href="#"><i class="fa fa-skype"></i></a><a class="social-01__item" href="#"><i class="fa fa-twitter"></i></a><a class="social-01__item" href="#"><i class="fa fa-instagram"></i></a>
												</nav>
											</div><!-- End / social-01 -->
											
									</div>
								</div><!-- End / textbox -->
								
								
								<!-- textbox -->
								<div class="textbox textbox__style-02">
									<div class="textbox__image"><a href="#"><img src="assets/img/avatars/4.jpg" alt=""/></a></div>
									<div class="textbox__body">
										<h2 class="textbox__title"><a href="#">Cynthia Aguilar</a></h2>
										<div class="textbox__description">Integer placerat ullamcorper urna nec rhoncus. Sed velit justo, lacinia non sapien imperdiet, sagitt</div>
											
											<!-- social-01 -->
											<div class="social-01">
												<nav class="social-01__navSocial"><a class="social-01__item" href="#"><i class="fa fa-facebook"></i></a><a class="social-01__item" href="#"><i class="fa fa-skype"></i></a><a class="social-01__item" href="#"><i class="fa fa-twitter"></i></a><a class="social-01__item" href="#"><i class="fa fa-instagram"></i></a>
												</nav>
											</div><!-- End / social-01 -->
											
									</div>
								</div><!-- End / textbox -->
								
							</div><!-- End / carousel__element owl-carousel -->
							
						</div>
					</div>
				</section>
				<!-- End / Section -->
				
				<!-- Latest Projects-->
				
				<!-- Section -->
				<section class="md-section" style="padding-bottom:0;">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-lg-8 offset-0 offset-sm-0 offset-md-2 offset-lg-2 ">
								
								<!-- title-01 -->
								<div class="title-01">
									<h2 class="title-01__title">Latest Projects</h2>
								</div><!-- End / title-01 -->
								
							</div>
						</div>
					</div>
					<div class="consult-project">
						<div class="row">
							<div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 " style="padding-left: 5px; padding-right: 5px;">
								
								<!-- post-02 -->
								<div class="post-02 post-02__style-02 js-post-effect">
									<div class="post-02__media"><a href="#"><img src="assets/img/projects/v-1.jpg" alt=""/></a></div>
									<div class="post-02__body">
										<h2 class="post-02__title"><a href="#">How to Create and Manage SVG Sprites</a></h2>
										<div class="post-02__department">Business Consulting</div>
										<div class="post-02__content">
											<div class="post-02__description">Etiam non varius justo, vel tempor mi. Nulla facilisi. Fusce at tortor arcu. Suspendisse maximus ac nisl eu porta. Praesent eget consequat nisi, at mollis turpis. Quisque sed venenatis neque, at molli</div><a class="post-02__link" href="#">View detail</a>
										</div>
									</div>
								</div><!-- End / post-02 -->
								
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 " style="padding-left: 5px; padding-right: 5px;">
								
								<!-- post-02 -->
								<div class="post-02 post-02__style-02 js-post-effect">
									<div class="post-02__media"><a href="#"><img src="assets/img/projects/v-2.jpg" alt=""/></a></div>
									<div class="post-02__body">
										<h2 class="post-02__title"><a href="#">Is UX Really That Important?</a></h2>
										<div class="post-02__department">Business Consulting</div>
										<div class="post-02__content">
											<div class="post-02__description">Nam elit ligula, egestas et ornare non, viverra eu justo. Aliquam ornare lectus ut pharetra dictum. Aliquam erat volutpat. In fringilla erat at eros pharetra faucibus. Nunc a magna eu lectus fringilla</div><a class="post-02__link" href="#">View detail</a>
										</div>
									</div>
								</div><!-- End / post-02 -->
								
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 " style="padding-left: 5px; padding-right: 5px;">
								
								<!-- post-02 -->
								<div class="post-02 post-02__style-02 js-post-effect">
									<div class="post-02__media"><a href="#"><img src="assets/img/projects/v-3.jpg" alt=""/></a></div>
									<div class="post-02__body">
										<h2 class="post-02__title"><a href="#">How to Create and Manage SVG Sprites</a></h2>
										<div class="post-02__department">Business Consulting</div>
										<div class="post-02__content">
											<div class="post-02__description">Suspendisse ac elit vitae est lacinia interdum eu sit amet mauris. Phasellus aliquam nisi sit amet libero mattis ornare. In varius nunc vel suscipit rhoncus. Nunc hendrerit nisl nec orci eleifend accu</div><a class="post-02__link" href="#">View detail</a>
										</div>
									</div>
								</div><!-- End / post-02 -->
								
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 " style="padding-left: 5px; padding-right: 5px;">
								
								<!-- post-02 -->
								<div class="post-02 post-02__style-02 js-post-effect">
									<div class="post-02__media"><a href="#"><img src="assets/img/projects/v-4.jpg" alt=""/></a></div>
									<div class="post-02__body">
										<h2 class="post-02__title"><a href="#">5 Ways to Use 404 Pages</a></h2>
										<div class="post-02__department">Business Consulting</div>
										<div class="post-02__content">
											<div class="post-02__description">Sed ante nisl, fermentum et facilisis in, maximus sed ipsum. Cras hendrerit feugiat eros, ut fringilla nunc finibus sed. Quisque vitae dictum augue, vitae pretium sem. Proin tristique lobortis mauris </div><a class="post-02__link" href="#">View detail</a>
										</div>
									</div>
								</div><!-- End / post-02 -->
								
							</div>
						</div>
					</div>
				</section>
				<!-- End / Section -->

		
				
				
		<!-- What’s Client Say ?-->

			@include('trade.design.section_testimonials') 	
				
		<!-- END What’s Client Say ?-->		

				
		<!-- Our partner-->

			@include('trade.design.section_partners') 	

		<!-- END Our partner-->		
				
				
				
				
			</div>
			<!-- End / Content-->
   
@endsection
