<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		
		<!-- Fonts   asset('storage/file.txt') -->
		<link rel="stylesheet" type="text/css" href=" {{ asset('trade') }}/assets/fonts/fontawesome/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('trade') }}/assets/fonts/themify-icons/themify-icons.css">
		
		<!-- Vendors-->
		<link rel="stylesheet" type="text/css" href="{{ asset('trade') }}/assets/vendors/bootstrap4/bootstrap-grid.min.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('trade') }}/assets/vendors/magnific-popup/magnific-popup.min.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('trade') }}/assets/vendors/owl.carousel/owl.carousel.css">
		<!-- App & fonts-->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i&amp;amp;subset=latin-ext">
		<link rel="stylesheet" type="text/css" href="{{ asset('trade') }}/assets/css/main.css"><!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<![endif]-->
	</head>
	
	<body>
		<div class="page-wrap">
			
	<!-- Site header -->
		@section('site_header')
			@include('trade.design.site_header') 
		@show
	<!-- END Site header  -->
			

	<!-- Content -->	
		@yield('content')
	<!-- END Content -->
	
			
	<!-- Footer -->
		@section('footer')
			@include('trade.design.footer') 
		@show
	<!-- END Footer -->		
			
		</div> <!-- END .page-wrap -->		


		<!-- Vendors-->
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/_jquery/jquery.min.js"></script>
		<script type="text/javascript" src=" {{ asset('trade') }}/assets/vendors/imagesloaded/imagesloaded.pkgd.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/isotope-layout/isotope.pkgd.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/jquery.countdown/jquery.countdown.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/jquery.countTo/jquery.countTo.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/jquery.countUp/jquery.countup.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/jquery.matchHeight/jquery.matchHeight.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/jquery.mb.ytplayer/jquery.mb.YTPlayer.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/masonry-layout/masonry.pkgd.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/owl.carousel/owl.carousel.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/jquery.waypoints/jquery.waypoints.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/menu/menu.min.js"></script>
		<script type="text/javascript" src="{{ asset('trade') }}/assets/vendors/smoothscroll/SmoothScroll.min.js"></script>
		<!-- App-->
		<script type="text/javascript" src="{{ asset('trade') }}/assets/js/main.js"></script>
	</body>
</html>