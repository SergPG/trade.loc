<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;


use App\Models\Theme;


class PageController extends FrontendController
{
    //
protected $modelTheme;
	
 public function __construct(Theme $modelTheme )
    {

        $this->modelTheme = $modelTheme;       
    }

	public function index($name){

  	$theme = $this->modelTheme->where('alias', '=', $name)->firstOrFail();

	//	dd($theme);

		return view('themes.'.$theme->alias.'.index')->with([
                        'theme' => $theme->alias,
                        ]);  
	}


}
