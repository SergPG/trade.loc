<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

use Route;

use Config;
use \Menu;


use App\Models\Slider;
use App\Models\Theme;

class HomeController extends FrontendController
{
    // -------------------
  protected $modelSlider;
  protected $modelTheme;

  public function __construct(Slider $modelSlider, Theme $modelTheme )
    {
        parent::__construct();


        $this->modelSlider = $modelSlider;
        $this->modelTheme = $modelTheme;
        
    }

  // ------------------
  public function index(){

  	$sliders = $this->modelSlider->all();

      $themes = $this->modelTheme->all();
    
      $menuMy = \Menu::get('MainMenuTop')->all();
//    $route = url()->current();
//    $route2 = url()->full();
 //   $route3 = url()->previous();


//    dd($route, $route2, $route3);


     $menus = Menu::all();
        
         // //  $menuMy = \Menu::get('MyNavBar4');

   //   dd($menus);
		
		// dd($sliders);

    //  \Menu::make('MyNavBar', function ($menu) {
    //     $menu->add('Home');
    //     $menu->add('About', 'about');
        
    // });

    $this->vars = array_add($this->vars,'sliders',$sliders);

		return view('trade.home.home')->with( $this->vars); 
	}




}
