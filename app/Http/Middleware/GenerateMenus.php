<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Theme;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

   protected $themes;

    public function handle($request, Closure $next)
    {
       
            $this->mainMenuTop();
       
             $this->adminMenuTop();

        return $next($request);
    }


// Main Menu Top Site Trade
      public function mainMenuTop(){

        $this->themes = Theme::all();
             
         \Menu::make('MainMenuTop', function ($menu) {
                  
            $menu->add('Главная')->nickname('home');
            $menu->add('О Нас', 'about')->nickname('about');
            $menu->add('Темы сайтов', '#')
                        ->nickname('pages_theme')
                        ->attr(['class' => 'menu-item-has-children']);
      
    foreach ($this->themes as $key => $theme) {
       $menu->get('pages_theme')
            ->add($theme->title, ['route'  => ['pages.show', 'theme' => $theme->alias]])
            ->nickname($theme->alias);
    }
            $menu->add('Контакты', '#')->nickname('contact');
   
          });

      } 
// END Main Menu Top Site Trade


// Admin Menu Top Site Trade
      public function adminMenuTop(){

        $this->themes = Theme::all();
             
         \Menu::make('AdminMenuTop', function ($menu) {
                  
            $menu->add('Dashbcard',['url' => 'admin'])->nickname('dashbcard');
            $menu->add('О Нас', '#')->nickname('about');
            
            $menu->add('Контакты', '#')->nickname('contact');
   
          });

      } 
// END Main Menu Top Site Trade


}
