<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\HomeController@index')->name('home');

Route::get('pages/{theme}', 'Frontend\PageController@index')->name('pages.show');

Route::get('/about', 'Frontend\AboutController@index')->name('about');

Route::get('/services', 'Frontend\ServiceController@index')->name('services');

Route::get('/services/{id}', 'Frontend\ServiceController@show')->name('service.show');



// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	//admin
	Route::get('/','Backend\DashboardController@index')->name('admin.dashboard');
	
	//Themes 
	Route::resource('themes','Backend\ThemeController');
	

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home1');
