<?php

//'active_class' => 'current-menu-item',  AdminMenuTop
return array(
    'default' => array(
        'auto_activate' => true,
        'activate_parents' => true,
        'active_class' => 'active',
        'restful' => false,
        'cascade_data' => true,
        'rest_base' => '',      // string|array
        'active_element' => 'item',  // item|link
    ),
	'mainmenutop' => array(
        'active_class' => 'current-menu-item',
    ),
	'adminmenutop' => array(
        'active_class' => 'current-menu-item',
    ),
);
